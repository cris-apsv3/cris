package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.dao.PublicationDAO;
import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.dao.ResearcherDAO;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = (String) request.getParameter("id");
		String title = (String) request.getParameter("title");
		String publicationName = (String) request.getParameter("publicationname");
		String publicationDate = (String) request.getParameter("publicationdate");
		String authors = (String) request.getParameter("authors");
		int citeCount = Integer.parseInt(request.getParameter("citecount"));
		
		
		PublicationDAO rdao = PublicationDAOImplementation.getInstance();
		Publication p = new Publication();
		
		p.setId(id);
		p.setTitle(title);
		p.setPublicationName(publicationName);
		p.setPublicationDate(publicationDate);
		p.setAuthors(authors);
		p.setCiteCount(citeCount);
		
		rdao.create(p);
		
		//request.getSession().setAttribute("user", user);
        response.sendRedirect(request.getContextPath() + "/AdminServlet");
		
	}

}
