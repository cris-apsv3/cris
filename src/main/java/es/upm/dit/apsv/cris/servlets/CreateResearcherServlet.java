package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.dao.ResearcherDAO;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;


@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = (String) request.getParameter("id");
		String name = (String) request.getParameter("name");
		String lastname = (String) request.getParameter("lastname");
		String email = (String) request.getParameter("email");
		
		
		ResearcherDAO rdao = ResearcherDAOImplementation.getInstance();
		Researcher r = new Researcher();
		
		r.setId(id);
		r.setName(name);
		r.setLastname(lastname);
		r.setEmail(email);
		
		rdao.create(r);
		
		//request.getSession().setAttribute("user", user);
        response.sendRedirect(request.getContextPath() + "/AdminServlet");
		
	}

}
